/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>        /* fprintf() fputs() fopen() fclose() */
#include <stdlib.h>       /* exit() free() */
#include <string.h>       /* memcmp() strlen() strtok() strdup() strerror() */
#include <ctype.h>        /* isblank() */
#include <errno.h>
#include "swarm/io.h"     /* afgets() */
#include "swarm/dict.h"
#include "swarm/sarray.h"
#include "tags.h"         /* get_tags() print_tags() tracktotal() */

static const char *cue_coms[] = \
  {"TRACK", "GENRE", "DATE", "ARTIST", "COMPOSER", "PERFORMER", "SONGWRITER",\
   "TITLE", "ISRC", NULL};

static void tok(dict *d, char* str);
static void normal(char *str, int length);
static void usage(FILE *fout);

int
main(int argc, char **argv)
{
  FILE *fcue   = NULL;
  char *in_str = NULL;
  dict *d      = NULL;
  sarray *sarr = NULL;
  tags *t      = NULL;
  long int tnum = 0, total = 0;

  if (argc == 1 || argc > 3) {
    usage(stdout);
    exit(1);
  }

  if ((fcue = fopen(argv[1], "r")) == NULL) {
    fprintf(stderr, "ERROR: fopen(): %s: %s\n", argv[1], strerror(errno));
    usage(stdout);
    exit(1);
  }

  d = dict_new();   /* needs free(d) */
  sarr = sarray_new();
  while ((in_str = afgets(fcue)) != NULL) {
    tok(d, in_str);
    sarray_add(sarr, in_str);   /* needs sarray_free(sarr, 1) */

    if (in_str == NULL) {
      fprintf(stderr, "ERROR: afgets() failed\n");
      abort();
    }
  }
  
  fclose(fcue);

  total = tracktotal(d);
  if (argv[2]
      &&
      ((tnum = strtol(argv[2], NULL, 10)) <= total
       &&
       tnum > 0)) {
    t = get_tags(d, (size_t)tnum);   /* needs free(t) */
    print_tags(t, total);
    free (t);
  } else {
    for (size_t i=1; i <= total; i++) {
      t = get_tags(d, i);   /* needs free(t) */
      print_tags(t, total);
      putchar('\n');
      free(t);
    }
  }

#ifdef DEBUG_PRINT
  dict_dbgprint(d);
#endif

  dict_free(d);
  sarray_free(sarr, 1);
  return 0;
}

static void
tok(dict *d, char* str)
{
  char *key = NULL;
  char *val = NULL;
  size_t len_key = 0, len_val = 0;

  const char* delim = " \t";   /* strtok() delimiters */

  key = strtok(str, delim);
  len_key = strlen(key);

  if (memcmp(key, "REM", len_key) == 0) {
    key = strtok(NULL, delim);
    if (key) {
      len_key = strlen(key);
    }
  }

  for(size_t i=0; cue_coms[i] != NULL; i++) {
    if (memcmp(key, cue_coms[i], len_key) == 0) {
      
      if (memcmp("TRACK", key, len_key) == 0)
	val = strtok(NULL, delim);
      else
	val = strtok(NULL, "");

      len_val = strlen(val);
      normal(val, len_val + 1);
      dict_add(d, key, val);      
    }
  }
}

static void
normal(char *str, int length)
{
  char *str_dup = NULL;
  size_t str_len = strlen(str) + 1;
  
  if ((str_dup = malloc(str_len)) == NULL) {
    fprintf(stderr, "ERROR: normal(): malloc() failed: %s\n", strerror(errno));
    abort();
  }
  
  strncpy(str_dup, str, str_len);
  
  /* start of line */
  for(size_t i=0; i < length; i++) {
    if (isblank(str_dup[i]) || str_dup[i] == '"') {
      continue;
    }
    else {
      strcpy(str, str_dup + i);
      break;
    }
  }

  /* end of line */
  for(size_t i=length; i-- > 0;) {
    if ( (str[i] == '"')
	 ||
	 (str[i] == '\r')
	 ||
	 (str[i] == '\n')
	 ) {
      str[i] = '\0';
    }
  }

  free(str_dup);
}

static void
usage(FILE *fout)
{
  fputs("USAGE:\n\tcuevor_print cuefile\n\
\tcuevor_print cuefile tracknumber\n\n\
DESRIPTION:\n\tcuevor_print prints tags to standard output.\n\n\
BUGS & FEATURES:\n\tUTF-16, UTF-32 are not supported.\n\n", fout);
}
