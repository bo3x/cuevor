/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef TAGS_H_SWARM
#define TAGS_H_SWARM

#include "swarm/dict.h"

typedef struct tags {
  char *album,
    *artist,
    *genre,
    *date,
    *performer,
    *title,
    *tracknumber,
    *isrc;
} tags;

long tracktotal(const dict *in);

/* get_tags() returns a pointer to a structure (tags). Memory is obtained with malloc().
 * The pointer returned by get_tags() should be passed to free().
 */
tags *get_tags(const dict *in, const long tnum);

void print_tags(const tags *t, const long total);

#endif
