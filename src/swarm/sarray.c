/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdlib.h>   /* malloc() realloc() free() abort() */
#include <stdio.h>    /* fprintf() */
#include <string.h>   /* strerror() */
#include <errno.h>
#include "sarray.h"

sarray *
sarray_new(void)
{
  sarray *out;
  if ( (out = malloc(sizeof(sarray))) == NULL) {
    fprintf(stderr, "ERROR: sarray.c: sarray_new(): malloc() failed: %s\n", strerror(errno));
    abort();
  }
  
  if (out) {
    *out = (sarray){};
  }

  return out;
}

sarray *
sarray_add(sarray *in, char *str)
{
  in->cnt++;
  if ((in->s = realloc(in->s, sizeof(char*)*in->cnt)) == NULL) {
    fprintf(stderr, "ERROR: sarray.c: sarray_add(): realloc() failed: %s\n", strerror(errno));
    abort();    
  }
  in->s[in->cnt-1] = str;
  return in;
}

void
sarray_print(sarray *in)
{
  for(size_t i=0; i < in->cnt; i++) {
    fprintf(stdout, "%s\n", in->s[i]);
  }
}

/* if  b  is not equal to zero, then the free() function will also be applied 
 * to each element (string) of the array.
 */
void
sarray_free(sarray *in, int b)
{
  if (b) {
    for (size_t i=0; i < in->cnt; i++)
      free(in->s[i]);
  }

  free(in->s);
  free(in);
}
