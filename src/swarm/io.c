/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>   /* fgets() fputc() */
#include <stdlib.h>  /* realloc() */
#include <string.h>  /* strlen() strcat() */
#include "io.h"

/* afgets() returns a pointer to a new string. The new string is allocated
 * using realloc(). The pointer returned by afgets() should be passed to free().
 * On error, afgets() returns NULL.
 */
char *
afgets(FILE *f)
{
  char *s = NULL;
  char tmp_s[SIZE] = {};
  size_t len_tmp=0, len_ttl=0;
  
  do {
    if (fgets(tmp_s, SIZE, f) != NULL) {
      len_tmp = strlen(tmp_s);
      len_ttl += len_tmp;

      if ( (s = realloc(s, len_ttl + 1)) != NULL ) {
	for (size_t i=len_tmp+1; i-- > 0; ) {
	  s[len_ttl - i]='\0';
	}
	strcat(s, tmp_s);
      } else {
	break;
      }
    } else {
      s=NULL; /* fgets() failed */
      break;
    }

  } while(len_tmp == SIZE-1 && tmp_s[SIZE-2] != '\n');

  return s;
}

/* fget_u8c() returns a utf8 code of a Unicode character (not a code point!) or 
 * END_OF_UTF8 on end of file or error.
 */
char32_t
fget_u8c(FILE *f)
{
  char32_t u8c = END_OF_UTF8;
  int c = 0; /* fgetc() returns int */
  size_t tail = 0;

  do {
    if ((c = fgetc(f)) == EOF) {
      break;
    };

    if ((c & (0x80)) == 0) { /* 1byte: 0xxxxxxx */
      ;
    } else if ((c & (0xF0)) == 0xF0) { /* 4bytes: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
      tail = 3;
    } else if ((c & (0xE0)) == 0xE0) { /* 3bytes: 1110xxxx 10xxxxxx 10xxxxxx */
      tail = 2;
    } else if ((c & 0xC0) == 0xC0) { /* 2bytes: 110xxxxx 10xxxxxx */
      tail = 1;
    } else {
      u8c = END_OF_UTF8;
    }

    u8c = (char32_t)c;
    for (size_t i=0; i < tail; i++) {
      if ((c = fgetc(f)) == EOF) {
	u8c = END_OF_UTF8;
	break;
      }
      if ((c & (0x80)) == 0x80) {
	u8c = u8c << 8;
	u8c = u8c | ((char32_t)c & 0xff);
      } else {
	u8c = END_OF_UTF8;
	break;
      }
    }
    
  } while(0);

  return u8c;
}

/* fput_u8c() writes the utf8 character 'u8c' to 'f'.
 * fput_u8c() returns 'u8c' on success, or 'EOF' on error.
 */
char32_t
fput_u8c(char32_t u8c, FILE *f)
{
  char32_t u8c_out = u8c;
  size_t n = 0;
  
  if ((u8c & 0x80) == 0) { /* 1byte */
    n = 1;
  } else if ((u8c & (0xC080)) == 0xC080) { /* 2bytes */
    n = 2;
  } else if ((u8c & (0xE08080)) == 0xE08080) { /* 3bytes */
    n = 3;
  } else if ((u8c & (0xF0808080)) == 0xF0808080) { /* 4bytes */
    n = 4;
  }

  /* reverse byte order */
  u8c = (u8c & 0x00FF00FF) << 8 | (u8c & 0xFF00FF00) >> 8;
  u8c = (u8c & 0x0000FFFF) << 16 | (u8c & 0xFFFF0000) >> 16;

  /* omit unneeded byte(s) */
  u8c = u8c >> (sizeof(char32_t)-n)*8;
  
  for (size_t i=0; i < n; i++) {
    if (fputc((unsigned char)(u8c >> i*8), f) == EOF) {
      u8c_out = EOF;
      break;
    }
  }
  return u8c_out;
}
