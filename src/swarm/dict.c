/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdlib.h>  /* malloc() realloc() free() abort() */
#include <stdio.h>   /* fprintf() */
#include <strings.h> /* strcasecmp() */
#include <string.h>  /* strerror() */
#include <errno.h>
#include "dict.h"

static void dict_add_keyval(dict *in, keyval *kv);

/***************/
/* keyval pair */

keyval *
keyval_new(char *key, void *val)
{
  keyval *out;
  
  if ((out = malloc(sizeof(keyval))) == NULL) {
    fprintf(stderr, "ERROR: dict.c: keyval_new(): malloc() failed: %s\n", strerror(errno));
    abort();
  }
  *out = (keyval){.key = key, .val = val};
  
  return out;
}

int
keyval_iskey(const keyval *in, const char *key)
{
  return !strcasecmp(in->key, key);
}

void
keyval_free(keyval *in)
{
  free(in);
}


/********/
/* dict */

dict *
dict_new(void)
{
  dict *out;
  
  if ((out = malloc(sizeof(dict))) == NULL) {
    fprintf(stderr, "ERROR: dict.c: dict_new(): malloc() failed: %s\n", strerror(errno));
    abort();
  }
  *out = (dict){};
  
  return out;
}

static void
dict_add_keyval(dict *in, keyval *kv)
{
  in->elms++;
  if ((in->pairs = realloc(in->pairs, sizeof(keyval*)*in->elms)) == NULL) {
    fprintf(stderr, "ERROR: dict.c: dict_add_keyval(): realloc() failed: %s\n", strerror(errno));
    abort();
  }
  in->pairs[in->elms-1] = kv;
}

void
dict_add(dict *in, char *key, void *val)
{
  if (!key) {
    fprintf(stderr, "ERROR: dict_add(): NULL is not a valid key.\n");
    abort();
  }

  dict_add_keyval(in, keyval_new(key, val));
}

void
dict_dbgprint(const dict *in)
{
  for (size_t i = 0; i < in->elms; i++) {
    fprintf(stdout,
	    "DBG*** : i = %lu; elms = %d; key=%s; value=%s\n",
	    i, in->elms, (char*)in->pairs[i]->key, (char*)in->pairs[i]->val);
  }
}

void
dict_free(dict *in)
{
  for (size_t i = 0; i < in->elms; i++)
    keyval_free(in->pairs[i]);
  
  free(in->pairs);
  free(in);
}
