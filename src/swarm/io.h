/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef IO_H_SWARM
#define IO_H_SWARM

#include <uchar.h>

#ifndef SIZE
# define SIZE 4096
#endif

#ifndef END_OF_UTF8
# define END_OF_UTF8 0xffffffffu
#endif

/* afgets() returns a pointer to a new string. The new string is allocated
 * using realloc(). The pointer returned by afgets() should be passed to free().
 * On error, afgets() returns NULL.
 */
char *afgets(FILE *f);


/* fget_u8c() returns a utf8 code of a Unicode character (not a code point!) or 
 * END_OF_UTF8 on end of file or error.
 */
char32_t fget_u8c(FILE *f);


/* fput_u8c() writes the utf8 character 'u8c' to 'f'.
 * fput_u8c() returns 'u8c' on success, or 'EOF' on error.
 */
char32_t fput_u8c(char32_t u8c, FILE *f);

#endif
