#!/bin/bash

# ISC License
#
# Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

usage () {
    echo -e "USAGE:\n\tcuevor.sh [OPTION] CUEFILE [MFILE]..."
    echo
    echo -e "DESCRIPTION:\n\tcuevor.sh adds tags to ogg and flac files" \
	 "based on cue information. cuevor.sh depends on cuevor_print."
    echo
    echo "OPTIONS:"
    echo -e "\t-i tracknumber,\n\t\tUse specific track number"
    echo
    echo "Supported formats, Requirements:"
    echo -e "\togg, vorbiscomment"
    echo -e "\tflac, metaflac"
    echo
    echo -e "EXAMPLES:\n\tcuevor.sh cool_music.cue *.ogg\n" \
	 "\tcuevor.sh cool_music.cue -i 8 08.trackname.flac"
    echo
    echo -e "BUGS & FEATURES:\n\tUTF-16, UTF-32 are not supported."
}

output_vars () {
    echo "***"
    echo "A total number of tracks: $tracktotal"
    echo "A total number of files: $n_mfiles"
    echo "Processed files: $procfiles"
}

checkerr () {
    if [ $? ]; then
	echo "DONE"
    else
	echo "Error"
	exit 1
    fi    
}

tagogg () {
    local oggfile="$1"
    
    echo -n "$oggfile ... "
    cuevor_print "$cuefile" "$tnum" | vorbiscomment -w "$oggfile"
    checkerr
    let ++procfiles
}

tagflac () {
    local flacfile="$1"

    echo -n "$flacfile ... "
    cuevor_print "$cuefile" "$tnum" | metaflac --remove-all-tags --import-tags-from=- "$flacfile"
    checkerr
    let ++procfiles
}

main () {
    while getopts ":i:" opt; do
	case $opt in
	    i)
		user_tnum=$OPTARG
		if [ -n "$user_tnum" ] && [ "$user_tnum" -eq "$user_tnum" ] 2>/dev/null; then
		    tnum=$((user_tnum - 1)) #because ++tnum
		else
		    echo "ERROR: $user_tnum: not a number"
		    exit 1
		fi
		
		;;
	    *)
		usage
		exit
		;;
	esac
    done
    shift $((OPTIND - 1))
    
    cuefile="$1"
    
    if [ $# -lt 1 ] || ! [ -f "$cuefile" ]; then
	usage
	exit 1
    fi
    
    tracktotal="$( cuevor_print "$cuefile" | grep -i -m 1 tracktotal | cut -d= -f 2 )"
    mfiles=("${@:2}")
    n_mfiles="${#mfiles[@]}"
    procfiles=0

    for i in "${mfiles[@]}"; do
	if [ ${tnum:=0} -lt $tracktotal ]; then
	    case "$i" in
		*.[Oo][Gg][Gg] )
		    let ++tnum
		    tagogg "$i"
		    ;;
		*.[Ff][Ll][Aa][Cc] )
		    let ++tnum
		    tagflac "$i"
		    ;;
		*.* )
		    echo "$i: unknown file type"
		    ;;
	    esac
	else
	    echo "$i: ERROR: tracktotal=$tracktotal ... exit ($tnum)"
	    output_vars
	    exit 3
	fi
	
	if [ -n "$user_tnum" ]; then
	    break
	fi
    done

    output_vars    
}

main "$@"

