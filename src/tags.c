/*
 * ISC License
 *
 * Copyright (c) 2019, Dmitriy Ivanov <bo3x@yandex.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <string.h>  /* strcmp() */
#include <stdio.h>   /* fprintf() */
#include <stdlib.h>  /* malloc() abort() */
#include <string.h>  /* strerror() */
#include <errno.h>
#include <stdbool.h>
#include "tags.h"

long
tracktotal(const dict *in)
{
  int total = 0;

  for (int i = 0; i < in->elms; i++) {
    if(keyval_iskey(in->pairs[i], "TRACK"))
      total++;
  }
  
  return total;
}

/* (cue)                    (vorbis)
 * REM DATE             ==> DATE +
 * REM GENRE            ==> GENRE +
 * TITLE (before TRACK) ==> ALBUM +
 * TITLE (after TRACK)  ==> TITLE +
 * PERFORMER            ==> ARTIST +
 * REM COMPOSER         ==> ARTIST +
 * TRACK №              ==> TRACKNUMBER +
 * ISRC                 ==> ISRC +
 * NULL                 ==> TRACKTOTAL +
 * ---
 * ???                  ==> PERFORMER -
 * SONGWRITER           ==> ???
*/

tags *
get_tags(const dict *in, const long tnum)
{
  static bool istrack = false;
  static char *album = NULL,
    *genre     = NULL,
    *date      = NULL,
    *performer = NULL,
    *artist    = NULL;
  size_t cnt = 0; /* track number */
  tags *t = NULL;
  
  if ((t = malloc(sizeof(tags))) == NULL) {
    fprintf(stderr, "ERROR: get_tags(): malloc() failed: %s\n", strerror(errno));
    abort();
  }
  
  *t = (tags){};
  
  for (size_t i=0; i < in->elms; i++) {
    if (!strcmp(in->pairs[i]->key, "TRACK") &&
	!istrack)
      istrack = true;
      
    if (!istrack) {   /* before track */
      if (!strcmp(in->pairs[i]->key, "TITLE"))
	album = in->pairs[i]->val;
      else if (!strcmp(in->pairs[i]->key, "GENRE"))
	genre = in->pairs[i]->val;
      else if (!strcmp(in->pairs[i]->key, "DATE"))
	date = in->pairs[i]->val;
      else if (!strcmp(in->pairs[i]->key, "PERFORMER") ||
	       !strcmp(in->pairs[i]->key, "COMPOSER"))
	artist = in->pairs[i]->val;
    } else {   /* after track */
      if (!strcmp(in->pairs[i]->key, "TRACK")) {
	  if (album)     t->album = album;
	  if (genre)     t->genre = genre;
	  if (date)      t->date = date;
	  if (performer) t->performer = performer;
	  if (artist)    t->artist = artist;
	  cnt++;
      }
      if (cnt == tnum) {
	if (!strcmp(in->pairs[i]->key, "TITLE"))
	  t->title = in->pairs[i]->val;
	else if (!strcmp(in->pairs[i]->key, "TRACK"))
	  t->tracknumber = in->pairs[i]->val;
	else if (!strcmp(in->pairs[i]->key, "ISRC"))
	  t->isrc = in->pairs[i]->val;
	else if (!strcmp(in->pairs[i]->key, "PERFORMER")  ||
		 !strcmp(in->pairs[i]->key, "COMPOSER"))
	  t->artist = in->pairs[i]->val;
      }
    }
  }
  return t;
}

void
print_tags(const tags *t, const long total)
{
  if (t->tracknumber) fprintf(stdout, "TRACKNUMBER=%s\n", t->tracknumber);
  if (total)          fprintf(stdout, "TRACKTOTAL=%lu\n", total);
  if (t->title)       fprintf(stdout, "TITLE=%s\n", t->title);
  if (t->performer)   fprintf(stdout, "PERFORMER=%s\n", t->performer);
  if (t->artist)      fprintf(stdout, "ARTIST=%s\n", t->artist);
  if (t->album)       fprintf(stdout, "ALBUM=%s\n", t->album);
  if (t->date)        fprintf(stdout, "DATE=%s\n", t->date);
  if (t->genre)       fprintf(stdout, "GENRE=%s\n", t->genre);
  if (t->isrc)        fprintf(stdout, "ISRC=%s\n", t->isrc);
}
