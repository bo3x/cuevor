# DESCRIPTION
Utility of adding tags to ogg and flac files based on cue information.

# INSTALLATION
```
make
make install
```

# List of files that will be installed
* **_cuevor.sh_** (adds tags to files)
* **_cuevor_print_** (prints tags to standard output)

# UNINSTALLATION
```
make uninstall
```

# LICENSE
[ISC license](https://www.isc.org/licenses/)

